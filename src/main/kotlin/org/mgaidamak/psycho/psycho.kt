package org.mgaidamak.psycho

import java.io.FileReader
import kotlin.streams.toList

fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)!!

fun main(args : Array<String>) {
    val keys = listOf(io_, id_, in_, ip_, im_, ic_, iz_).map { Key.parse(it) }.toList()

    val u2 = ClassLoader.getSystemResource("people.csv")
    val people = FileReader(u2.file).readLines().stream().map { People.parse(it) }.toList()

    println("ii;io;id;in;ip;im;ic;iz")

    people.forEach { p ->
        print(p.p)
        keys.forEach { k ->
            val s = Calc.sum(k, p)
            val d = k.formula(s.toDouble())
            print(";${d.format(2)}")
        }
        println("")
    }
}