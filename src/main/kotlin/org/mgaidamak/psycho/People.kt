package org.mgaidamak.psycho

import kotlin.streams.toList

data class People(val p: String, val s: List<Int>) {
    companion object {
        fun parse(l:String) : People {
            val ps = l.split(';')
            return People(ps[0], ps.subList(1, ps.size).stream().map {
                when {
                    it.isNullOrEmpty() ->-1
                    else -> it.toInt()
                }
            }.toList())
        }
    }
}