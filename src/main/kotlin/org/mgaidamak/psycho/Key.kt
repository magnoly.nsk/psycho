package org.mgaidamak.psycho

import kotlin.streams.toList

const val io_ = "io;0;1;0;1;0;0;0;0;0;1;1;0;1;1;0;1;0;1;0;0;1;0;1;0;1;0;1;1;0;1;0;1;1;0;1;0;0;1;0;1"
const val id_ = "id;0;;;;0;;;;;;1;;1;;;;;;;;;;1;;;;;1;;;;1;1;;;;;;;"
const val in_ = "in;;;;1;;;;;;;;;;;;1;;;;0;;;;;;;1;;0;;;;;;;0;;1;;1"
const val ip_ = "ip;0;;;;;;;;0;;;;1;;0;;;1;;;1;;;;;0;1;1;;;0;1;;;1;0;0;1;;1"
const val im_ = "im;;1;;1;;0;0;;;;;0;1;;;1;;;;;1;0;1;0;;0;;;0;;;;1;0;;;0;;;"
const val ic_ = "ic;;1;;1;;;0;;;;;0;;;;1;;;;;;0;;;;;;;;;;;1;;;;0;;;"
const val iz_ = "iz;;;0;;;;;0;;;;;;1;;;0;;0;;;;;;1;;;;;1;0;;;;;;;;;"

data class Key(val k: String, val s: List<Int>) {
    companion object {
        fun parse(l: String) : Key {
            val ks = l.split(';')
            return Key(ks[0], ks.subList(1, ks.size).stream().map {
                when {
                    it.isNullOrEmpty() ->-1
                    else -> it.toInt()
                }
            }.toList())
        }
    }

    fun formula(x: Double): Double {
        return when(k) {
            "io" -> (x-24.3)/6.7
            "id" -> (x-5.1)/1.9
            "in" -> (x-4.8)/1.9
            "ip" -> (x-9.6)/3.1
            "im" -> (x-9.1)/3
            "ic" -> (x-4.5)/1.7
            "iz" -> (x-4.9)/1.7
            else -> -255.0
        }
    }
}