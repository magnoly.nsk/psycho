package org.mgaidamak.psycho

class Calc {
    companion object {
        fun sum(k: Key, p: People) : Int {
            var sum = 0
            for (i in 0 until p.s.size) {
                if (k.s[i]!=-1 && p.s[i]==k.s[i]) {
                    sum+=1
                }
            }
            return sum
        }
    }
}