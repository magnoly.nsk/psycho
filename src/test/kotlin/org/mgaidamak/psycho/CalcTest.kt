package org.mgaidamak.psycho

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class CalcTest {

    companion object {
        val p0 = People.parse("1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0")
        val p1 = People.parse("2;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1")

        val pio = People.parse(io_)
        val io = Key.parse(io_)
        val pid = People.parse(id_)
        val id = Key.parse(id_)
        val pin = People.parse(in_)
        val inn = Key.parse(in_)
        val pip = People.parse(ip_)
        val ip = Key.parse(ip_)
        val pim = People.parse(im_)
        val im = Key.parse(im_)
        val pic = People.parse(ic_)
        val ic = Key.parse(ic_)
        val piz = People.parse(iz_)
        val iz = Key.parse(iz_)
    }

    @Test
    fun checkIo() {
        assertEquals (21, Calc.sum(io, p0),"IO with all 0 answers")
        assertEquals (19, Calc.sum(io, p1),"IO with all 1 answers")
        assertEquals (40, Calc.sum(io, pio),"IO with the same answers")
    }

    @Test
    fun checkId() {
        assertEquals (2, Calc.sum(id, p0),"ID with all 0 answers")
        assertEquals (6, Calc.sum(id, p1),"ID with all 1 answers")
        assertEquals (8, Calc.sum(id, pid),"ID with the same answers")
    }

    @Test
    fun checkIn() {
        assertEquals (3, Calc.sum(inn, p0),"IN with all 0 answers")
        assertEquals (5, Calc.sum(inn, p1),"IN with all 1 answers")
        assertEquals (8, Calc.sum(inn, pin),"IN with the same answers")
    }

    @Test
    fun checkIp() {
        assertEquals (7, Calc.sum(ip, p0),"IP with all 0 answers")
        assertEquals (9, Calc.sum(ip, p1),"IP with all 1 answers")
        assertEquals (16, Calc.sum(ip, pip),"IP with the same answers")
    }

    @Test
    fun checkIm() {
        assertEquals (9, Calc.sum(im, p0),"IM with all 0 answers")
        assertEquals (7, Calc.sum(im, p1),"IM with all 1 answers")
        assertEquals (16, Calc.sum(im, pim),"IM with the same answers")
    }

    @Test
    fun checkIc() {
        assertEquals (4, Calc.sum(ic, p0),"IC with all 0 answers")
        assertEquals (4, Calc.sum(ic, p1),"IC with all 1 answers")
        assertEquals (8, Calc.sum(ic, pic),"IC with the same answers")
    }

    @Test
    fun checkIz() {
        assertEquals (5, Calc.sum(iz, p0),"IZ with all 0 answers")
        assertEquals (3, Calc.sum(iz, p1),"IZ with all 1 answers")
        assertEquals (8, Calc.sum(iz, piz),"IZ with the same answers")
    }
}